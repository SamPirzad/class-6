package sbu.cs.ship;

public class App {

    public static void main(String[] args) {
        Company company = new Company("raymon");
        company.addShip(new Ship("00", Color.GREEN, 100));
        company.addShip(new Ship("01", Color.GREEN, 83));
        company.addShip(new Ship("02", Color.BLUE, 45));
        company.addShip(new Ship("03", Color.RED, 111));
        company.addShip(new Ship("04", Color.GREY, 119));
        company.addShip(new Ship("05", Color.PINK, 200));

        Customer ali = new Customer("ali", "000");
        Customer sadegh = new Customer("sadegh", "001");
        Customer sahar = new Customer("sahar", "002");

        sahar.reserve(Color.PINK, company, 3);
        ali.reserve(Color.GREEN, company, 2);
        ali.reserve(Color.GREY, company, 2);
        sadegh.reserve(Color.PINK, company, 5);
        sadegh.reserve(Color.GREEN, company, 2);
        sadegh.reserve(Color.GREEN, company, 2);
    }
}
