package sbu.cs.ship;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Contract {

    private Customer customer;
    private double price;
    private Date date;
    private int days;
    private Ship ship;

    public Contract(Customer customer, double price, Date date, Ship ship, int days) {
        this.customer = customer;
        this.price = price;
        this.date = date;
        this.ship = ship;
        this.days = days;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public Ship getShip() {
        return ship;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }

    public void checkEnd() {
        long diff = new Date().getTime() - this.date.getTime();
        if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) >= this.days) {
            this.ship.setAvailable(true);
        }
    }
}
